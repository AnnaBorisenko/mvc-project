<?php
require_once __DIR__. '/Validate.php';
require_once __DIR__. '/Log.php';
$logger = new Log('logging/logging.log');
if (isset($_POST) && !empty($_POST)) {
    $validate = new Validate();
    $logger->info(json_encode(['incomming input'=> $_POST]));
    $file = fopen('users.json', 'r');
    $emails = [];
    $users = [];
    if ($file) {
        $contents = fread($file, filesize('users.json'));
        fclose($file);
        $users = json_decode($contents, true);
        $emails = array_column($users, 'email');
    } else {
        $logger->error(json_encode('Users data not found'));
        http_response_code(404);
        exit (json_encode(["errors" => 'Users data not found']));
    }
    $rules = [
        'firstName' => [
            'display' => 'First name',
            'required' => true,
        ],
        'lastName' => [
            'display' => 'Last name',
            'required' => true,
        ],
        'email' => [
            'display' => 'Email address',
            'required' => true,
            'valid_email' => true,
            'unique' => ['emailValue'=>$_POST['email'], 'emails' => $emails]
        ],
        'password' => [
            'display' => 'Password',
            'required' => true,
            'matches' => 'repeatPassword',
        ],
        'repeatPassword' => [
            'display' => 'Repeat password',
            'required' => true,
            'matches' => 'password',
        ],
    ];
    $validate->check($_POST, $rules);
    $errors = $validate->errors();
    if (!empty($errors)) {
        $logger->error(json_encode(['Validation failed with errors' => $errors]));
        http_response_code(403);
        exit (json_encode(['errors' => $errors]));
    }
    array_push($users, $_POST);
    $file = 'users.json';
    file_put_contents($file, '');
    file_put_contents($file, json_encode($users), FILE_APPEND);
    http_response_code(200);
    $logger->info(json_encode(['new user recorded with data' => $_POST]));
    exit (json_encode(['message' => "User registered successfully"]));
} else {
    http_response_code(400);
    $logger->error('Input fields failed');
    http_response_code(404);
    exit (json_encode(['errors' => "Input fields is missing or empty"]));
}
