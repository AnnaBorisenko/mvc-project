<?php
require_once __DIR__. '/Input.php';
class Validate
{
    private $_passed = false, $_errors = [];

    public function check($source, $items = [])
    {
        $this->_errors = [];
        foreach ($items as $item => $rules) {
            $item = Input::sanitize($item);
            $display = $rules['display'];
            foreach ($rules as $rule => $rule_value) {
                $value = Input::sanitize(trim($source[$item]));
                if ($rule === 'required' && empty($value)) {
                    $this->addError([$item => "{$display} is required"]);
                } else if (!empty($value)) {
                    switch ($rule) {
                        case 'matches':
                            if ($value != $source[$rule_value]) {
                                $matchDisplay = $items[$rule_value]['display'];
                                $this->addError([$item => "{$matchDisplay} and {$display} must mutch."]);
                            }
                            break;

                        case 'unique':
                            if (in_array($rule_value['emailValue'], $rule_value['emails'])) {
                                $this->addError([$item => "{$display} already exists. Please choose another {$display}"]);
                            }
                            break;
                        case 'valid_email':
                            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                                $this->addError([$item => "{$display} must be a valid email address." ]);
                            }
                            break;
                    }
                }
            }
        }

        if (empty($this->_errors)) {
            $this->_passed = true;
        }

        return $this;
    }

    public function addError($error)
    {
        $this->_errors[] = $error;
        if (empty($this->_errors)) {
            $this->_passed = true;
        } else {
            $this->_passed = false;
        }
    }

    public function errors()
    {
        return $this->_errors;
    }

    public function passed()
    {
        return $this->_passed;
    }
}