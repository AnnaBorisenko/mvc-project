class Validator {
    constructor() {
        this.errors = {};
    }

    required(field, value, display) {
        if (!value || value === "") {
            this.errors[field] = display + " is required.";
            return false;
        }
        return true;
    }

    email(field, value) {
        const pattern = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (!pattern.test(value)) {
            if (!Object.keys(this.errors).includes(field)) {
                this.errors[field] = "Please enter a valid email address.";
            }
            return false;
        }
        return true;
    }

    passwordMatch(field, passwordValue, passwordRepeatValue) {
        if (passwordValue !== passwordRepeatValue) {
            this.errors[field] = "The password fields do not match.";
            return false;
        }
        return true;
    }

    getErrors() {
        return this.errors;
    }

    clearErrors() {
        this.errors = {};
    }
}


$(document).ready(function () {

    const validator = new Validator();
    const inputFields = $('input');

    inputFields.on('input', function () {
        const fieldName = $(this).attr('id');
        hideError(fieldName);
    });

    $('#submit-btn').on('click', function (e) {
        e.preventDefault();
        let validated = validate();
        if (Object.keys(validated.errors).length > 0) {
            for (let field of Object.keys(validated.errors)) {
                displayError(field, validated.errors[field]);
            }
        } else {
            let data = validated.fields;
            var jq = jQuery.noConflict();
            jq.ajax({
                type: 'POST',
                url: 'submitForm.php',
                data: data,
                success: function (response) {
                    $("#sending_form").attr('hidden', 'hidden');
                    let successAlert = $('#sending_form_success_alert');
                    successAlert.text(JSON.parse(response).message);
                    successAlert.removeAttr('hidden');
                },
                error: function (jqXHR) {
                    let errors = JSON.parse(jqXHR.responseText);
                    for (let errorField of errors.errors){
                        displayError(Object.keys(errorField)[0], Object.values(errorField)[0]);
                    }

                }
            });
        }
    });

    function validate() {
        const firstNameField = $('#firstName');
        const displayFirstNameField = firstNameField.closest('div').find('label').text();
        const firstNameValue = firstNameField.val();
        const lastNameField = $('#lastName');
        const displayLastNameField = lastNameField.closest('div').find('label').text();
        const lastNameValue = lastNameField.val();
        const emailField = $('#email');
        const displayEmailField = emailField.closest('div').find('label').text();
        const emailValue = emailField.val();
        const passwordField = $('#password');
        const displayPasswordField = passwordField.closest('div').find('label').text();
        const passwordValue = passwordField.val();
        const passwordRepeatField = $('#repeatPassword');
        const displayPasswordRepeatField = passwordRepeatField.closest('div').find('label').text();
        const passwordRepeatValue = passwordRepeatField.val();

        validator.clearErrors();
        validator.required('firstName', firstNameValue, displayFirstNameField);
        validator.required('lastName', lastNameValue, displayLastNameField);
        validator.required('email', emailValue, displayEmailField);
        validator.required('password', passwordValue, displayPasswordField);
        validator.required('repeatPassword', passwordRepeatValue, displayPasswordRepeatField);
        validator.passwordMatch('repeatPassword', passwordValue, passwordRepeatValue);
        validator.email('email', emailValue);

        return {
            errors: validator.getErrors(), fields: {
                firstName: firstNameValue,
                lastName: lastNameValue,
                email: emailValue,
                password: passwordValue,
                repeatPassword: passwordRepeatValue
            }
        };
    }

    function displayError(field, errorText) {
        $('#' + field).addClass('hasError');
        const fieldAlert = $('#' + field + 'Alert');
        fieldAlert.text(errorText);
        fieldAlert.removeAttr('hidden');
    }

    function hideError(field) {
        $('#' + field).removeClass('hasError');
        const fieldAlert = $('#' + field + 'Alert');
        fieldAlert.attr('hidden', 'hidden');
    }


});