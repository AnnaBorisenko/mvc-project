<?php
class Log
{
    private $file;

    public function __construct($filename) {
        if (!file_exists($filename)) {
            file_put_contents($filename, '');
        }
        $this->file = fopen($filename, 'a');
    }

    private function write($mode, $message) {
        $timestamp = date('Y-m-d H:i:s');
        $formatted_message = "[$timestamp] $mode.: $message\n";
        fwrite($this->file, $formatted_message);
    }

    public function info($message)
    {
        $this->write('INFO', $message);
    }

    public function error($message)
    {
        $this->write('ERROR', $message);
    }


    public function __destruct() {
        fclose($this->file);
    }

}